'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(()=>{
  Route.post('/project/register','LenderProjectController.registerProject').middleware('auth','lender').validator('RegisterProject')
  Route.get('/project','LenderProjectController.getAllProject').middleware('auth','lender')

  Route.post('/project/lend','TransactionController.sendEthForClient').validator('LendEth')
  Route.post('/project/lend/token','TransactionController.sendTokenForClient').validator('LendEth')

  Route.post('/lend','EthController.sendEthForClient').validator('LendEth')
  Route.post('/lend/token','EthController.sendTokenForClient').validator('LendEth')


  Route.get('/:environment/project/balance/:uuid','TransactionController.getAssetBalance').middleware('auth','lender')
  Route.get('/:environment/project/history/:uuid','LenderProjectController.getLendingHistory').middleware('auth','lender')



  Route.get('/project/wallet/:uuid','LenderProjectController.showEthereumAccount').middleware('auth','lender')
  Route.put('/project/wallet/:uuid','LenderProjectController.updateEthereumAccount').middleware('auth','lender').validator('UpdateEthereumAccount')


}).prefix('api/v1/lender')
