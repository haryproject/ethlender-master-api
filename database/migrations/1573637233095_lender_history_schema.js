'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LenderHistorySchema extends Schema {
  up () {
    this.create('lender_histories', (table) => {
      table.increments()
      table.string('symbol')
      table.string('uuid')
      table.string('transactionHash')
      table.string('amount')
      table.string('to')
      table.timestamps()
    })
  }

  down () {
    this.drop('lender_histories')
  }
}

module.exports = LenderHistorySchema
