'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LenderProjectSchema extends Schema {
  up () {
    this.create('lender_projects', (table) => {
      table.increments()
      table.string('uuid')
      table.string('project_name')
      table.enu('network',['mainnet','rinkeby','ropsten','kovan','private'])
      table.string('private_network_url',)
      table.string('mnemonic')
      table.string('private_key')
      table.string('public_key')
      table.string('address')
      table.integer('userId').unsigned().references('id').inTable('master_users')
      table.timestamps()
    })
  }

  down () {
    this.drop('lender_projects')
  }
}

module.exports = LenderProjectSchema
