'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class MasterAuthToken extends Model {
}

module.exports = MasterAuthToken
