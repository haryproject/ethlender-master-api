'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SystemUserPrivilege extends Model {

  credential() {
    return this.hasMany('App/Models/Credential/PrivilegeGroup', 'privilegeGroupId', 'id')
  }

}

module.exports = SystemUserPrivilege
