'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class MasterUser extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Credential/MasterAuthToken')
  }
  wallet() {
    return this.belongsTo('App/Models/MasterEthereumWallet', 'id', 'userId')
  }

  privilege(){
    return this.hasMany('App/Models/Credential/SystemUserPrivilege','id','user_id')
  }

  kyc(){
    return this.belongsTo('App/Models/LedgerAccountKyc','id','user_id')
  }


  static get hidden(){
    return ['created_at','updated_at','id','password']
  }
}

module.exports = MasterUser
