'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LenderProject extends Model {
  static get hidden(){
    return ['created_at', 'updated_at', 'id', 'mnemonic','public_key','private_key']
  }
}

module.exports = LenderProject
