'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WalletConfig extends Model {
  static get hidden(){
    return ['paypal_client_id','paypal_secret','created_at','updated_at','id','local_address','live_address','mandatory','lender_uuid','lender_address','decimal','gas_price']
  }
}

module.exports = WalletConfig
