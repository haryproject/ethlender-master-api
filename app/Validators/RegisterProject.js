'use strict'

class RegisterProject {
  get rules () {
    return {
      project_name:'required',
      network:'required|in:mainnet,rinkeby,ropsten,kovan,private',
    }
  }

  get messages () {
    return {
      'project_name.required':'Project name is required',
      'network.required':'Network is required',
      'network.in':'Network must between mainnet,rinkeby,ropsten,kovan,private',
    }
  }
}

module.exports = RegisterProject
