'use strict'

class UpdateEthereumAccount {
  get rules() {
    return {
      address: "required",
      private_key:"required",
      public_key:"required",
      mnemonic:"required"
    }
  }

  get messages(){
    return {
      "address.required": "Address is required",
      "private_key.required":"Private Key is required",
      "public_key.required":"Public Key is required",
      "mnemonic.required":"Mnemonic is required"
    }
  }
}

module.exports = UpdateEthereumAccount
