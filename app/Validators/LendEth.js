'use strict'

class LendEth {
  get rules() {
    return {
      uuid: 'required',
      receiver: 'required',
      amount: 'required'
    }
  }

  get messages() {
    return {
      'uuid.required': 'UUID must not be empty',
      'receiver.required': 'Receiver must not be empty',
      'amount.required': 'Amount must not be empty'
    }
  }
}

module.exports = LendEth
