'use strict'
const {Wallet} = use('ethers')
const Project = use('App/Models/LenderProject')
const uuidv4 = require('uuid/v4');
const Encryption = use('Encryption')
const {ethConnector, ethConnectorLocal} = use('App/Helper/EthereumHelper')
const LendingHistory = use('App/Models/LenderHistory')
const WalletConfiguration = use('App/Models/WalletConfig')


class LenderProjectController {

  async registerProject({request, response, auth}) {
    const wallet = new Wallet.createRandom()
    const address = wallet.signingKey.address

    const uuid = uuidv4()
    const {mnemonic, privateKey, publicKey} = wallet.signingKey
    const {project_name, network, private_network_url} = request.all()
    const encryptedMnemonic = Encryption.encrypt(mnemonic)
    const encryptedPrivateKey = Encryption.encrypt(privateKey)
    const encryptedPublicKey = Encryption.encrypt(publicKey)
    const userId = auth.user.id


    return await Project.create({
      userId,
      uuid,
      project_name,
      network,
      private_network_url,
      mnemonic: encryptedMnemonic,
      private_key: encryptedPrivateKey,
      public_key: encryptedPublicKey,
      address
    })
  }

  async getAllProject({request, response, auth}) {
    const userId = auth.user.id
    const projectList = await Project.query().select('uuid', 'project_name', 'network', 'address').where({userId}).fetch()
    if (!projectList) return response.notFound({message: "Project not found"})
    return projectList
  }

  async getLendingHistory({request, response, auth, params}) {
    const {uuid} = params
    const history = await LendingHistory.query().where({uuid}).orderBy('created_at', 'desc').fetch()
    return history
  }

  async showEthereumAccount({request, response, auth, params}) {
    const {uuid} = params
    const project = await Project.findBy({uuid})
    if (project.userId !== auth.user.id) return response.forbidden({message: "This project is not belong to you"})

    let {address, mnemonic, private_key, public_key} = project
    mnemonic = Encryption.decrypt(mnemonic)
    private_key = Encryption.decrypt(private_key)
    public_key = Encryption.decrypt(public_key)

    return {address, mnemonic, private_key, public_key}
  }

  async updateEthereumAccount({request, response, auth, params}) {
    const {uuid} = params
    const {address, mnemonic, private_key, public_key} = request.all()
    const project = await Project.findBy({uuid})

    if (project.userId !== auth.user.id) return response.forbidden({message: "This project is not belong to you"})
    project.mnemonic = Encryption.encrypt(mnemonic)
    project.private_key = Encryption.encrypt(private_key)
    project.public_key = Encryption.encrypt(public_key)
    project.address = address

    await project.save()
    return {message: "Wallet account updated"}

  }



}

module.exports = LenderProjectController
