'use strict'
const {Wallet} = use('ethers')
const Project = use('App/Models/LenderProject')
const uuidv4 = require('uuid/v4');
const Encryption = use('Encryption')
const {ethConnector, ethConnectorLocal, contractConnector, contractConnectorLocal} = use('App/Helper/EthereumHelper')
const LendingHistory = use('App/Models/LenderHistory')
const WalletConfiguration = use('App/Models/WalletConfig')


class EthController {

  async sendEthForClient({request, response, auth}) {
    console.log("Someone need an ethereum")
    const {uuid, receiver, amount, eth_network} = request.all()
    const project = await Project.findBy({uuid})

    const {private_key, address} = project
    const decryptionPrivateKey = Encryption.decrypt(private_key)

    const web3 = ethConnector(eth_network, decryptionPrivateKey)

    const sendEthTxHash = await web3.eth.sendTransaction({
      from: address,
      to: receiver,
      value: await web3.utils.toWei(amount, 'ether')
    }).on('error', async function (error) {
      console.log(error)
    }).on('receipt', async function (receipt) {
      return receipt.transactionHash
    })


    const history = await LendingHistory.create({
      uuid,
      symbol: "ETH",
      transactionHash: sendEthTxHash.transactionHash,
      amount,
      to: receiver
    })

    return history
  }

  async sendTokenForClient({request, response, auth}) {

    const {uuid, receiver, contract_address, amount, gasprice, eth_network} = request.all()
    const project = await Project.findBy({uuid})

    const {private_key, address, mnemonic} = project
    const decryptionMnemonic = Encryption.decrypt(mnemonic)


    const contract = contractConnector(eth_network, decryptionMnemonic, contract_address, gasprice)
    const tokenAmount = parseFloat(amount * (10 ** tokenConfig.decimals)).toFixed(0)


    const sendTokenTxHash = await contract.methods.transfer(
      receiver,
      tokenAmount.toString()
    ).send({from: address,})
      .on('error', async function (error) {
        console.log(error)
      }).on('receipt', async function (receipt) {
        return receipt.transactionHash
      })


    const history = await LendingHistory.create({
      uuid,
      symbol: tokenConfig.symbol,
      transactionHash: sendTokenTxHash.transactionHash,
      amount,
      to: receiver
    })

    return history
  }


  async getAssetBalance({request, response, auth, params}) {
    const {uuid} = params
    const {environment} = params
    const userId = auth.user.id

    const project = await Project.findBy({uuid})
    const isTheirProject = (userId === project.userId)
    if (!isTheirProject) return response.forbidden({message: "This merchant is not yours"})

    const {private_key, address, mnemonic} = project
    const decryptionPrivateKey = Encryption.decrypt(private_key)
    const decryptionMnemonic = Encryption.decrypt(mnemonic)
    const tokenConfig = await WalletConfiguration.findBy({type: "token", mandatory: '1'})
    const web3 = environment === 'live' ? ethConnector(tokenConfig.live_network_url, decryptionPrivateKey) : ethConnectorLocal(tokenConfig.local_network_url, decryptionPrivateKey)
    const lendeEthBalanceWei = await web3.eth.getBalance(address)

    const contract = environment === 'live' ? contractConnector(tokenConfig.live_network_url, decryptionMnemonic, tokenConfig.live_address, tokenConfig.gas_price) : contractConnectorLocal(tokenConfig.local_network_url, decryptionMnemonic, tokenConfig.local_address, tokenConfig.gas_price)
    const lenderTokenBalanceWei = await contract.methods.balanceOf(address).call()
    const balance = parseFloat((lenderTokenBalanceWei / (10 ** tokenConfig.decimals)).toFixed(4))

    return {eth: web3.utils.fromWei(lendeEthBalanceWei, 'ether'), token: balance + ' ' + tokenConfig.symbol}
  }


}

module.exports = EthController
