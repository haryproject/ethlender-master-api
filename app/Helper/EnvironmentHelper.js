const {ethConnector, ethConnectorLocal, contractConnector, contractConnectorLocal} = use('App/Helper/EthereumHelper')

module.exports.ethereum = async function (environment,url,mnemonic) {
  const isLive = environment === 'live'
  return isLive ? await ethConnector(url,mnemonic) : await ethConnectorLocal(url,mnemonic)
}

module.exports.contract = async function (environment,url,mnemonic, contract_address, gas_price) {
  const isLive = environment === 'live'
  return isLive ? await contractConnector(url,mnemonic, contract_address, gas_price) :
    await contractConnectorLocal(url,mnemonic, contract_address, gas_price)
}
