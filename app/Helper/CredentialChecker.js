module.exports.runProperUserChecker = async function (user, ecosystemNeeded, authorityNeeded) {

  const privilege = await user.privilege().fetch()
  const userPrivilegeId = privilege.toJSON().map(data => data.privilegeGroupId)


  const userAcceptedAuthority = userPrivilegeId.filter(authority => authorityNeeded.includes(authority))
  const hasAuthority = userAcceptedAuthority.length > 0


  return hasAuthority

}

module.exports.isBanned = async function (user) {

  const privilege = await user.privilege().fetch()
  const userPrivilegeId = privilege.toJSON().map(data => data.privilegeGroupId)


  const authorityNeeded = [999]
  const userAcceptedAuthority = userPrivilegeId.filter(authority => authorityNeeded.includes(authority))
  const hasAuthority = userAcceptedAuthority.length > 0


  return hasAuthority

}
