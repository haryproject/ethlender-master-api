'use strict'

const CredentialChecker = use('App/Helper/CredentialChecker')
const {Ecosystem, Authority} = use('App/Helper/MasterCredential')

class LenderAccount {

  async handle ({ request ,response,auth}, next) {
    const user = auth.user
    const isCredentialCorrect = await CredentialChecker.runProperUserChecker(user,Ecosystem.LENDER,[Authority.LENDER])
    if(!isCredentialCorrect) return response.forbidden({message:"You're not authorized to do this action"})

    await next()
  }
}

module.exports = LenderAccount
