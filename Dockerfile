FROM node:10

WORKDIR /usr/src/app


COPY package*.json ./

RUN npm i -g @adonisjs/cli
RUN npm install

COPY . .

EXPOSE 3333

CMD ["adonis","serve" ]
